// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import java.util.Map;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ShotUtil extends SubsystemBase {
  /** Creates a new ShotUtil. */
  private CANSparkMax primaryShot, secondaryShot;
  private RelativeEncoder shotEncoder;
  private WPI_VictorSPX innerIndex, outerIndex, leftBrush, rightBrush;
  private ShuffleboardTab tab = Shuffleboard.getTab("Diagnostics");
  private ShuffleboardLayout motorControl, motorStatus, shotData, shotIdleMode;
  private NetworkTableEntry shotSpeed, innerIndexSpeed, outerIndexSpeed, brushSpeed, zeroAllMotors, shotVelocity, shotSetpoint, shotCurrent, toggleShotMode, shotIdleModeDisplay, brushIsRunning, innerIndexIsRunning, outerIndexIsRunning;
  double shotMotorSpeed, innerIndexMotorSpeed, outerIndexMotorSpeed, brushMotorSpeed;
  boolean zeroed;

  public ShotUtil() {
    // Shuffleboard Config
    motorControl = tab.getLayout("ShotUtil: Motor Control", BuiltInLayouts.kList).withSize(3, 6).withPosition(7, 0);
    shotData = tab.getLayout("ShotUtil: Shot Motor Data", BuiltInLayouts.kList).withSize(3, 3).withPosition(10, 0);
    motorStatus = tab.getLayout("ShotUtil: Motor Status", BuiltInLayouts.kList).withSize(3, 3).withPosition(10, 3);
    shotSpeed = motorControl.add("Shot Speed", 0).withWidget(BuiltInWidgets.kNumberSlider).getEntry();
    innerIndexSpeed = motorControl.add("Inner Index Speed", 0).withWidget(BuiltInWidgets.kNumberSlider).getEntry();
    outerIndexSpeed = motorControl.add("Outer Index Speed", 0).withWidget(BuiltInWidgets.kNumberSlider).getEntry();
    brushSpeed = motorControl.add("Brush Speed", 0).withWidget(BuiltInWidgets.kNumberSlider).getEntry();
    zeroAllMotors = motorControl.add("Zero All Motors", false).withWidget(BuiltInWidgets.kToggleButton).getEntry();
    shotVelocity = shotData.add("Shot Velocity", 0).withWidget(BuiltInWidgets.kTextView).getEntry();
    shotSetpoint = shotData.add("Shot Setpoint", 0.0).withWidget(BuiltInWidgets.kNumberBar).getEntry();
    shotIdleMode = shotData.getLayout("Idle Mode", BuiltInLayouts.kList).withProperties(Map.of("Label position", "HIDDEN"));
    shotIdleModeDisplay = shotIdleMode.add("Idle Mode", "Brake").withWidget(BuiltInWidgets.kTextView).getEntry();
    toggleShotMode = shotIdleMode.add("Toggle Shot Idle Mode", false).withWidget(BuiltInWidgets.kToggleButton).getEntry();
    shotCurrent = shotData.add("Output Current (Amps)", 0.0).withWidget(BuiltInWidgets.kTextView).getEntry();
    brushIsRunning = motorStatus.add("Brush Running", false).withWidget(BuiltInWidgets.kBooleanBox).getEntry();
    innerIndexIsRunning = motorStatus.add("Inner Index Running", false).withWidget(BuiltInWidgets.kBooleanBox).getEntry();
    outerIndexIsRunning = motorStatus.add("Outer Index Running", false).withWidget(BuiltInWidgets.kBooleanBox).getEntry();
  

    // Motor Controller Config
    primaryShot = new CANSparkMax(Constants.primaryShot, MotorType.kBrushless);
    secondaryShot = new CANSparkMax(Constants.secondaryShot, MotorType.kBrushless);
    shotEncoder = primaryShot.getEncoder();
    innerIndex = new WPI_VictorSPX(Constants.innerIndexMotor);
    outerIndex = new WPI_VictorSPX(Constants.outerIndexMotor);
    leftBrush = new WPI_VictorSPX(Constants.leftBrushMotor);
    rightBrush = new WPI_VictorSPX(Constants.rightBrushMotor);
    primaryShot.setIdleMode(IdleMode.kBrake);
    secondaryShot.follow(primaryShot, true);
    innerIndex.setInverted(true);
    outerIndex.setInverted(true);
    leftBrush.setInverted(false);
    rightBrush.setInverted(true);
    rightBrush.follow(leftBrush);
  }

  public void runShotMotors(){
    shotMotorSpeed = shotSpeed.getDouble(0);
    primaryShot.set(shotMotorSpeed); 
  }

  public void runInnerIndexMotor(){
    innerIndexMotorSpeed = innerIndexSpeed.getDouble(0);
    innerIndex.set(innerIndexMotorSpeed);
    if (innerIndexMotorSpeed != 0) {
      innerIndexIsRunning.setBoolean(true);
    } else {
      innerIndexIsRunning.setBoolean(false);
    }
  }

  public void runOuterIndexMotor(){
    outerIndexMotorSpeed = outerIndexSpeed.getDouble(0);
    outerIndex.set(outerIndexMotorSpeed);
    if (outerIndexMotorSpeed != 0) {
      outerIndexIsRunning.setBoolean(true);
    } else {
      outerIndexIsRunning.setBoolean(false);
    }
  }

  public void runBrushMotor(){
    brushMotorSpeed = brushSpeed.getDouble(0);
    leftBrush.set(brushMotorSpeed);
    if (brushMotorSpeed != 0) {
      brushIsRunning.setBoolean(true);
    } else {
      brushIsRunning.setBoolean(false);
    }
  }

  public void zeroAll(){
    if (zeroAllMotors.getBoolean(false) == true){
      brushSpeed.setDouble(0);
      innerIndexSpeed.setDouble(0);
      outerIndexSpeed.setDouble(0);
      shotSpeed.setDouble(0);
      zeroAllMotors.setBoolean(false);
    }
  }

  public void toggleShotIdleMode(){
    if (toggleShotMode.getBoolean(false) == true){
      if (primaryShot.getIdleMode() == IdleMode.kCoast){
        primaryShot.setIdleMode(IdleMode.kBrake);
        shotIdleModeDisplay.setString("Brake");
      } else {
        primaryShot.setIdleMode(IdleMode.kCoast);
        shotIdleModeDisplay.setString("Coast");
      }
      toggleShotMode.setBoolean(false);
    }
  }

  public void runMotors(){
    runShotMotors();
    runBrushMotor();
    runInnerIndexMotor();
    runOuterIndexMotor();
    zeroAll();
    toggleShotIdleMode();
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    shotVelocity.setDouble(shotEncoder.getVelocity());
    shotSetpoint.setDouble(shotMotorSpeed);
    shotCurrent.setDouble(primaryShot.getOutputCurrent());
  }
}
